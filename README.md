# Minecraft Development Environment

This repository helps you setup a Development environment for Minecraft using IntelliJ and Docker. In this repository, you can quickly get started with a templitized Minecraft plugin and test it locally via a spigot minecraft server running in a docker container.


# Pre-requisites 

1. It's assumed you have [Docker installed](https://docs.docker.com/get-docker/) on your local computer. 

2. It's assumed you are using IntelliJ IDEA for development IDE with the [Minecraft Development Plugin](https://plugins.jetbrains.com/plugin/8327-minecraft-development) installed. If not, you can follow [these instructions](https://www.jetbrains.com/help/idea/managing-plugins.html) to search for the plugin and install it. Make sure to restart IntelliJ for the plugin to take effect.

3. It's recommended you have installed [Homebrew](https://brew.sh/) package manager on your Mac installed. Otherwise install it via the command in your terminal: 

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

4. You have installed Java 8 JDK on your Mac. If not, install it after installing Homebrew via the command:

```bash
brew update && brew cask install adoptopenjdk/openjdk/adoptopenjdk8
```

# Quick Start

0. Clone this repository locally via the command `git clone <https.....>`. Make sure to reference this repository's clone Https URL. 

1. Open up IntelliJ IDE on your local computer. 

2. Click `Create New Project`

	![new_project](images/new_proj.png)

3. Select `Minecraft` on the left pane. Select `Spigot Plugin` in the center pane. Select `1.8 Java Version...` in the drop down near the top of the screen. Click `Next` to continue. 

	![new_spigot_project](images/new_spigot_proj.png)

4. Next, configure your build settings. Enter in desired values for your plugin's `GroupId`, `ArtifactId`, and `Version`. Click `Next` to continue. 

	![build_settings](images/build_settings.png)

5. Next fill out your class and plug in information. 

	![spitgot_settings](images/spigot_settings.png)

6. Lastly, save this project within this directory. In this example the directory `hellowworld_plugin` has already been created for you. You can delete it and save your plug in project or you can skip the prior steps (1-5) and `import` the example helloworld project within IntelliJ instead of creating a new one. 

7. Check out [this tutorial](https://www.spigotmc.org/wiki/creating-a-plugin-with-maven-using-intellij-idea/#creating-the-main-plugin-class) and [this e-book](https://media.pragprog.com/titles/ahmine/build.pdf) to learn how to write your first `helloWorld` plugin. The main files you'll need to update will be [`helloWorld.java`](./helloworld_plugin/src/main/java/user/plugin/helloWorld.java) and  [`plugin.yml`](./helloworld_plugin/src/main/resources/plugin.yml)

8. Once you have written your plugin, next you need to compile it into a `.JAR` file. Expand the `Maven` tab on the right side of IntelliJ IDE. Next, click on the `helloWorld` package and then click the green play icon to `Run` the build command.
  
  * Once completed, you'll see a folder with the name `target` created in your project directory. Within that folder, you will find your `plugin-XXXX.jar` file. 
	
	![spitgot_settings](images/compile_jar.png)

9. Once you have your plugin compiled, copy the `.jar` file into [`data/plugins/`](data/plugins) directory for the local minecraft server. Within your local terminal, navigate to this folder's directory and enter the following command to start the minecraft server: 

```
docker-compose up
```

You will see a bunch of log output from that command which will download and install a Minecraft Spigot container for you locally in a docker container. The output will look something like this: 

```
spigot-mc_1  | [init] Running as uid=1000 gid=1000 with /data as 'drwxrwxr-x   20 1000     1000           640 May 30 06:54 /data'
spigot-mc_1  | [init] Resolved version given LATEST into 1.15.2
spigot-mc_1  | [init] Resolving type given SPIGOT
spigot-mc_1  | [init] Downloading Spigot from https://cdn.getbukkit.org/spigot/spigot-1.15.2.jar ...
spigot-mc_1  | [init] server.properties already created, skipping
spigot-mc_1  | [init] Checking for JSON files.
spigot-mc_1  | [init] Setting initial memory to 1G and max to 1G
spigot-mc_1  | [init] Starting the Minecraft server...
spigot-mc_1  | Loading libraries, please wait...
```

>**Note:**
>  * Make sure you have added yourself to whitelist before starting your minecraft server by updating the [`whitelist.json`](data/whitelist.json) file with you username and UUID. 
>  * Make sure you have updated the [`ops.json`](data/ops.json) file before starting your minecraft server so you have administrative access in-game on the server. 
>  * If it is your first time starting the server, it may take a few minutes. Please be patient. 

10. You can now connect to the server via the server address of : `127.0.0.1:25565` You will know the server is running by the log output showing something like this: 

```
spigot-mc_1  | [06:57:11] [Server thread/INFO]: Time elapsed: 8066 ms
spigot-mc_1  | [06:57:11] [Server thread/INFO]: Server permissions file permissions.yml is empty, ignoring it
spigot-mc_1  | [06:57:11] [Server thread/INFO]: Done (138.482s)! For help, type "help"
spigot-mc_1  | [06:57:11] [Server thread/INFO]: Starting remote control listener
spigot-mc_1  | [06:57:11] [RCON Listener #1/INFO]: RCON running on 0.0.0.0:25575
```

11. You can stop the server by pressing the `CTRL + C` keys within the terminal window you issued the `docker-compose` command. 



